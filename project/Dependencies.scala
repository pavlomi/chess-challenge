import sbt._

object Dependencies {
  val scalaTestV  = "3.0.5"
  val scalaMeterV = "0.8.2"
  val scoptV      = "3.7.0"

  val scalaTest  = "org.scalatest"     %% "scalatest"  % scalaTestV % Test
  val scalaMeter = "com.storm-enroute" %% "scalameter" % scalaMeterV
  val scopt      = "com.github.scopt"  %% "scopt"      % scoptV

  lazy val chessChallengeDependencies = Seq(scalaTest, scalaMeter, scopt)
}
