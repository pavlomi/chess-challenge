name := "chess-challenge"
version := "0.1"
scalaVersion := "2.12.6"

javaOptions in run ++= Seq("-Xms256M", "-Xmx2G", "-XX:+UseConcMarkSweepGC")
libraryDependencies ++= Dependencies.chessChallengeDependencies

fork in run := true
logBuffered := false

testFrameworks += new TestFramework("org.scalameter.ScalaMeterFramework")
logBuffered := false
parallelExecution in Test := false
