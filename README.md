# Chess challenge
## Problem
The problem is to find all unique configurations of a set of normal chess pieces on a chess board with
dimensions M×N where none of the pieces is in a position to take any of the others.
## Run sbt
```scala
run -c 3 -r 3 -p K,K,R
```

## Test

```scala
sbt test

```