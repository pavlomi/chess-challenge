package chess.challenge

import chess.challenge
import chess.engine.piece.Piece
import chess.engine.{Board, Position}

class ChessChallenge(board: Board, pieces: List[Piece]) {

  private lazy val start: Set[Layout] = Set(challenge.Layout(Map.empty[Position, Piece], board.positions))

  def compute(): Set[Layout] =
    if (pieces.isEmpty) Set.empty[Layout]
    else
      pieces.foldLeft(start) {
        case (layouts, piece) => layouts.flatMap(_.setPiece(piece, board))
      }
}
