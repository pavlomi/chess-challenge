package chess.challenge

import chess.engine.piece.Piece
import chess.engine.{Board, Position}

case class Layout(piecesPositions: Map[Position, Piece], availablePositionsOnBoard: Set[Position]) {
  def setPiece(piece: Piece, board: Board): Set[Layout] =
    availablePositionsOnBoard.flatMap { position =>
      val occupiedPositions  = piecesPositions.keys.toSet
      val availableMovements = piece.movements(position, board)
      val crosses            = (occupiedPositions intersect availableMovements)

      if (crosses.nonEmpty) None
      else Some(Layout(piecesPositions.updated(position, piece), (availablePositionsOnBoard diff availableMovements) filterNot (_ == position)))
    }

}
