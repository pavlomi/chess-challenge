package chess.engine.piece

import chess.engine.{Board, Move, Position}

case object King extends Piece {

  def movements(position: Position, board: Board): Set[Position] = {
    val movements = Move.cross ++ Move.diagonal

    movements.map(_(position)).filter(board.isLegalPosition)
  }
}
