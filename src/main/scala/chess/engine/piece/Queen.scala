package chess.engine.piece

import chess.engine.{Board, Move, Position}

case object Queen extends Piece {

  def movements(position: Position, board: Board): Set[Position] = {
    val movements = Move.cross ++ Move.diagonal

    movements.flatMap(board.movingToBorder(position))
  }
}
