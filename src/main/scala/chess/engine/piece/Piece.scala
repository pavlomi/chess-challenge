package chess.engine.piece

import chess.engine.{Board, Position}

trait Piece {
  def movements(position: Position, board: Board): Set[Position]
}
