package chess.engine.piece

import chess.engine.{Board, Move, Position}

case object Knight extends Piece {

  private val movements = Set(
    (Move.up _ andThen Move.upRight _),
    (Move.up _ andThen Move.upLeft _),
    (Move.right _ andThen Move.upRight _),
    (Move.right _ andThen Move.downRight _),
    (Move.down _ andThen Move.downRight _),
    (Move.down _ andThen Move.downLeft _),
    (Move.left _ andThen Move.downLeft _),
    (Move.left _ andThen Move.upLeft _)
  )

  def movements(position: Position, board: Board): Set[Position] =
    movements.flatMap { f =>
      val newPosition = f(position)
      if (board.isLegalPosition(newPosition)) Set(newPosition)
      else Set.empty[Position]
    }
}
