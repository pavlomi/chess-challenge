package chess.engine.piece

import chess.engine.{Board, Move, Position}

case object Rook extends Piece {
  def movements(position: Position, board: Board): Set[Position] = {
    val movements = Move.cross
    movements.flatMap(board.movingToBorder(position))
  }
}
