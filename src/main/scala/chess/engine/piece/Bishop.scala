package chess.engine.piece

import chess.engine.{Board, Move, Position}

case object Bishop extends Piece {
  def movements(position: Position, board: Board): Set[Position] = {
    val movements = Move.diagonal

    movements.flatMap(board.movingToBorder(position))
  }
}
