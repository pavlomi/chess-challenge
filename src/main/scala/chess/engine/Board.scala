package chess.engine

import scala.annotation.tailrec

case class Board(columns: Int, rows: Int) {

  lazy val positions: Set[Position] = (1 to columns).flatMap(c => (1 to rows).map(r => Position(c, r))).toSet

  def isLegalPosition(position: Position): Boolean = positions contains position

  def movingToBorder(start: Position)(move: Position => Position): Set[Position] = {

    @tailrec
    def loop(position: Position, acc: Set[Position]): Set[Position] =
      if (!isLegalPosition(position)) acc
      else loop(move(position), acc + position)

    val newPosition = move(start)
    if (newPosition == start) Set.empty[Position] else loop(newPosition, Set.empty[Position])
  }
}
