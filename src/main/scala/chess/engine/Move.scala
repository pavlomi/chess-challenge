package chess.engine

object Move {
  def up(position: Position): Position    = position.copy(row = position.row + 1)
  def down(position: Position): Position  = position.copy(row = position.row - 1)
  def right(position: Position): Position = position.copy(column = position.column + 1)
  def left(position: Position): Position  = position.copy(column = position.column - 1)

  def upRight(position: Position): Position   = (up _ andThen right _)(position)
  def upLeft(position: Position): Position    = (up _ andThen left _)(position)
  def downRight(position: Position): Position = (down _ andThen right _)(position)
  def downLeft(position: Position): Position  = (down _ andThen left _)(position)

  val cross: Set[Position => Position]    = Set(up _, down _, right _, left _)
  val diagonal: Set[Position => Position] = Set(upRight _, upLeft _, downRight _, downLeft _)
}
