package chess.engine

case class Position(column: Int, row: Int)
