package chess.interface

import chess.engine.piece.{Bishop, King, Knight, Piece, Queen}

case class Input(
  columns: Int = 7,
  rows: Int = 7,
  pieces: Seq[Piece] = Seq(King, King, Queen, Queen, Bishop, Bishop, Knight)
)
