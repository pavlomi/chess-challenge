package chess.interface

import chess.engine.piece._
import scopt._

object ArgumentParser extends OptionParser[Input]("chess_challenge") {

  implicit private val pieceFromString: Read[Piece] = Read.reads {
    case "K" => King
    case "Q" => Queen
    case "R" => Rook
    case "B" => Bishop
    case "N" => Knight
    case unknownPiece =>
      throw new IllegalArgumentException(s"$unknownPiece is not support piece.")
  }

  head("Chess challenge")

  opt[Int]('c', "columns")
    .action((columns, input) => input.copy(columns = columns))
    .text("Number of columns")

  opt[Int]('r', "rows")
    .action((rows, input) => input.copy(rows = rows))
    .text("Number of rows")

  opt[Seq[Piece]]('p', "pieces")
    .action((pieces, input) => input.copy(pieces = pieces))
    .text("Pieces. ([K]ing, [Q]ueen, [R]ook, [B]ishop, k[N]ight)")

}
