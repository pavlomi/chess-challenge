package chess.interface

import java.util.concurrent.TimeUnit

import chess.challenge.ChessChallenge
import chess.engine.Board
import chess.utils.Benchmark

class InterfaceAdapter(input: Input) {
  val board          = new Board(input.columns, input.rows)
  val chessChallenge = new ChessChallenge(board, input.pieces.toList)

  def show(): Unit = {
    val (layouts, benchmarkResult) = Benchmark.measure(chessChallenge.compute)

    println(s"Count of unique configurations: " + layouts.size)

    val spentTimeInSeconds = TimeUnit.SECONDS.convert(benchmarkResult.spentTime, TimeUnit.NANOSECONDS)

    println(s"Total time: $spentTimeInSeconds seconds or ${benchmarkResult.spentTime} nanoseconds")
  }
}
