package chess

import chess.interface.{ArgumentParser, Input, InterfaceAdapter}

object Boot extends App {
  val inputOpt = ArgumentParser.parse(args, Input())

  inputOpt match {
    case Some(input) => (new InterfaceAdapter(input)).show()
    case None        => println("Wrong arguments.")
  }

}
