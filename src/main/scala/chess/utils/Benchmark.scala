package chess.utils

case class BenchmarkResult(spentTime: Long)

object Benchmark {

  def measure[A](f: => A): (A, BenchmarkResult) = {
    val start             = System.nanoTime()
    val computationResult = f
    val end               = System.nanoTime()

    val benchmarkResult = BenchmarkResult(end - start)

    (computationResult, benchmarkResult)
  }
}
