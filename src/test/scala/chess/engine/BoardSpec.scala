package chess.engine

import org.scalatest.FlatSpec

class BoardSpec extends FlatSpec {
  val board = new Board(3, 3)
  val positions = Set(
    Position(1, 1), Position(1, 2), Position(1, 3),
    Position(2, 1), Position(2, 2), Position(2, 3),
    Position(3, 1), Position(3, 2), Position(3, 3)
  )

  "positions" must "return all positions of board" in {
    assert(board.positions == positions)
  }

  "isLegalPosition" must "return true for positions" in {
    assert(positions.forall(board.isLegalPosition))
  }

  "isLegalPosition" must "return false for Position(3, 4)" in {
    assert(board.isLegalPosition(Position(3, 4)) == false)
  }

  "movingToBorder" must "return list of positions between current position and border" in {
    assert(board.movingToBorder(Position(1, 1))(p => p.copy(row = p.row + 1)) == Set(Position(1, 3), Position(1, 2)))
  }

  "movingToBorder" must "return empty list" in {
    assert(board.movingToBorder(Position(1, 1))(p => p) == Set.empty[Position])
  }

  "movingToBorder" must "return list of position for diagonal movements" in {
    assert(board.movingToBorder(Position(1, 3))(p => p.copy(column = p.column + 1, row = p.row - 1)) == Set(Position(3, 1), Position(2, 2)))
  }
}
