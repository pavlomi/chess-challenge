package chess.engine.piece

import chess.engine.{Board, Position}
import org.scalatest.FlatSpec

class BishopSpec extends FlatSpec {

  behavior of "Bishop"

  val board = new Board(3, 3)

  "movements" must "return possible movements for provide position" in {
    assert(Bishop.movements(Position(3, 3), board) == Set(Position(1, 1), Position(2, 2)))
  }

  "movements" must "return possible movements for center position of board" in {
    assert(Bishop.movements(Position(2, 2), board) == Set(Position(3, 3), Position(1, 3), Position(3, 1), Position(1, 1)))
  }
}
