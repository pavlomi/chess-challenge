package chess.engine.piece

import chess.engine.{Board, Position}
import org.scalatest.FlatSpec

class KnightSpec extends FlatSpec {
  behavior of "Knight"

  val board = new Board(3, 3)

  "movements" must "return possible movements for provide position" in {
    assert(Knight.movements(Position(3, 3), board) == Set(Position(2, 1), Position(1, 2)))
  }

  "movements" must "return possible movements for center position of board" in {
    assert(Knight.movements(Position(2, 2), board) == Set.empty[Position])
  }
}
