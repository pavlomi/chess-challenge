package chess.engine.piece

import chess.engine.{Board, Position}
import org.scalatest.FlatSpec

class QueenSpec extends FlatSpec {
  behavior of "Queen"

  val board = new Board(3, 3)

  "movements" must "return possible movements for provide position" in {
    val positions = Set(Position(3, 1), Position(3, 2), Position(1, 3), Position(2, 3), Position(1, 1), Position(2, 2))
    assert(Queen.movements(Position(3, 3), board) == positions)
  }

  "movements" must "return possible movements for center position of board" in {
    val positions = Set(Position(2, 3), Position(2, 1), Position(3, 2), Position(1, 2), Position(3, 3), Position(1, 3), Position(3, 1), Position(1, 1))
    assert(Queen.movements(Position(2, 2), board) == positions)
  }
}
