package chess.engine.piece

import chess.engine.{Board, Position}
import org.scalatest.FlatSpec

class KingSpec extends FlatSpec {

  behavior of "King"

  val board = new Board(3, 3)

  "movements" must "return possible movements for provide position" in {
    assert(King.movements(Position(3, 3), board) == Set(Position(3, 2), Position(2, 3), Position(2, 2)))
  }

  "movements" must "return possible movements for center position of board" in {
    val positions = Set(Position(2, 3), Position(2, 1), Position(3, 2), Position(1, 2), Position(3, 3), Position(1, 3), Position(3, 1), Position(1, 1))
    assert(King.movements(Position(2, 2), board) == positions)
  }
}
