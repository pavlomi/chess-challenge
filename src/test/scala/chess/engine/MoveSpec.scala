package chess.engine

import org.scalatest.FlatSpec

class MoveSpec extends FlatSpec {
  behavior of "Move"

  val position = Position(2, 2)

  "up" should "increment row filed" in {
    assert(Move.up(position) == position.copy(row = position.row + 1))
  }

  "down" should "decrement row filed" in {
    assert(Move.down(position) == position.copy(row = position.row - 1))
  }

  "right" should "increment column filed" in {
    assert(Move.right(position) == position.copy(column = position.column + 1))
  }

  "left" should "decrement column filed" in {
    assert(Move.left(position) == position.copy(column = position.column - 1))
  }
}
