package chess.challenge

import chess.engine.{Board, Position}
import chess.engine.piece.{King, Piece, Rook}
import org.scalatest.FlatSpec

class ChessChallengeSpec extends FlatSpec {
  behavior of "chessChallenge"

  val board = new Board(3, 3)

  "compute" should "return list unique configurations" in {
    val pieces         = List(King, King, Rook)
    val chessChallenge = new ChessChallenge(board, pieces)

    val configuration1 = Layout(Map(Position(1, 1) -> King, Position(1, 3) -> King, Position(3, 2) -> Rook), Set.empty[Position])
    val configuration2 = Layout(Map(Position(1, 1) -> King, Position(3, 1) -> King, Position(2, 3) -> Rook), Set.empty[Position])
    val configuration3 = Layout(Map(Position(1, 3) -> King, Position(3, 3) -> King, Position(2, 1) -> Rook), Set.empty[Position])
    val configuration4 = Layout(Map(Position(3, 1) -> King, Position(3, 3) -> King, Position(1, 2) -> Rook), Set.empty[Position])
    val configurations = Set(configuration1, configuration2, configuration3, configuration4)
    assert(chessChallenge.compute() == configurations)
  }

  "compute" should "return empty list" in {
    val chessChallenge = new ChessChallenge(board, List.empty[Piece])
    assert(chessChallenge.compute().isEmpty)
  }
}
